# What is this?

It's an ansible configuration plus notes and a guide (see `install.md`) on how to set up an aws account so that you can work as a developer from anywhere with any laptop as long as you have an ssh key.

# Read `install.md`
