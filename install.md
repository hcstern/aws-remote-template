# Create server on amazon

## Create ec2 instance
- Use official debian stretch AMI <https://wiki.debian.org/Cloud/AmazonEC2Image>
	- TODO: current stable is now debian 10/buster, upgrade when AMI is released?
- Create private key locally
- Upload public key in ec2 -> key pairs
- Add admin account alias to .ssh/config

---

# Manual steps before install

## Update the sshd config (on the remote server)

I do this separately because it would be hard to coordinate the change with ansible given that you also have to simultaneously edit the local config.
Typically this would be something you'd include in a kickstart/anaconda/whatever config. 

- change port in server `sshd_config`
- change port in ssh/config file

## Change the vars in the ansible `hosts` file (locally in this directory)
- You should change the username and comment/personal name to your own
- Set the host to the ssh host alias you created in .ssh/config for the admin account

## Copy over any public keys into `roles/user/files/ssh_keys`
- Copy from .ssh/\*pub
- Ideally you should have one keypair per pair of (local, remote) machines

## Copy over any configs into `roles/user/files/bash_configs`
- Currently only set up to install bashrc, bash\_profile, and vimrc
- You can add more in `roles/user/tasks/configs.yml`

## Optionally, you can also apt upgrade (on the remote server):

- `sudo apt update`
- `sudo apt upgrade`

---

# Run the ansible playbook

## Run ansible playbook
`ansible-playbook --become remote-playbook.md`

---

# Manual steps after install

Add normal user alias in .ssh/config with normal user public keys 
